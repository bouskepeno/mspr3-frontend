import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ErpService, Employe, Employes} from '../services/erp-service';

@Component({
  selector: 'app-employes',
  templateUrl: './employes.component.html',
  styleUrls: ['./employes.component.css']
})
export class EmployesComponent implements OnInit {
  employes: Employes;

  constructor( private router: Router, private erpService: ErpService) { }

  ngOnInit(): void {
    this.erpService.getEmployes().subscribe(
      result => {
        this.employes = result;
      }
    );
  }

  retour(): void{
    this.router.navigateByUrl('erp/rh');
  }

}
