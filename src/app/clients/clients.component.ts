import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ErpService, Client, Clients} from '../services/erp-service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
clients: Clients;

  constructor(private router: Router, private erpService: ErpService) { }

  ngOnInit(): void {
    this.erpService.getClients().subscribe(
      result => {
        this.clients = result;
      }
    );
  }

  retour(): void{
    this.router.navigateByUrl('erp/rh');
  }

}
