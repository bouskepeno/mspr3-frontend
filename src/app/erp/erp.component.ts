import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-erp',
  templateUrl: './erp.component.html',
  styleUrls: ['./erp.component.css']
})
export class ErpComponent {

  constructor(private router: Router) { }

  goToRh(): void{
    this.router.navigateByUrl('erp/rh');
  }

}
