import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {  MatListModule} from '@angular/material/list';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RessourcesHumainesComponent } from './ressources-humaines/ressources-humaines.component';
import { ErpComponent } from './erp/erp.component';
import { EmployesComponent } from './employes/employes.component';
import { ClientsComponent } from './clients/clients.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    RessourcesHumainesComponent,
    ErpComponent,
    EmployesComponent,
    ClientsComponent
  ],
  imports: [
    BrowserModule,
    MatListModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
