import { Component} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ressources-humaines',
  templateUrl: './ressources-humaines.component.html',
  styleUrls: ['./ressources-humaines.component.css']
})
export class RessourcesHumainesComponent{

  constructor(private router: Router) { }

  retour(): void{
    this.router.navigateByUrl('/erp');
  }

  employes(): void{
    this.router.navigateByUrl('erp/rh/employes');
  }

  clients(): void{
    this.router.navigateByUrl('erp/rh/clients');
  }

}
