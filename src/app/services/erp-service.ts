import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface Employe{
  id: number;
  nom: string;
  prenom: string;
  age: number;
  fonction: string;
  adresse: string;
  ville: string;
  cp: number;
  mail: string;
  tel: string;
  photo: string;
}

export interface Client{
  id: number;
  nom: string;
  prenom: string;
  age: number;
  profession: string;
  adresse: string;
  ville: string;
  cp: number;
  mail: string;
  tel: string;
  photo: string;
}

export interface Clients{
  clients: Client[];
}

export interface Employes{
  employees: Employe[];
}

@Injectable({
  providedIn: 'root'
})
export class ErpService {

  constructor(private readonly httpClient: HttpClient) { }

  getEmployes(): Observable<Employes> {
    return this.httpClient.get<Employes>('http://localhost:8083/employee');
  }

  getClients(): Observable<Clients> {
    return this.httpClient.get<Clients>('http://localhost:8083/client');
  }
}
