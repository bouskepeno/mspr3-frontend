import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RessourcesHumainesComponent } from './ressources-humaines/ressources-humaines.component';
import { ErpComponent } from './erp/erp.component';
import { EmployesComponent } from './employes/employes.component';
import { ClientsComponent } from './clients/clients.component';

const routes: Routes = [
  { path: '', redirectTo: '/erp', pathMatch: 'full' },
  { path: 'erp', component: ErpComponent },
  { path: 'erp/rh', component: RessourcesHumainesComponent},
  { path: 'erp/rh/clients', component: ClientsComponent },
  { path: 'erp/rh/employes', component: EmployesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
